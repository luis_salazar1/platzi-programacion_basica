var palabra = 'japonesa';
palabra = palabra.toUpperCase();
var usuario;
// Array para almacenar letras introducidas por usuario
var letrasUsuario = Array();
for (i in palabra) {
  letrasUsuario.push('_');
}

var Ahorcado = function(contexto) {

  this.contexto = contexto;
  this.maximo = 5;
  this.intentos = 0;
  this.vivo = true;

  this.dibujar();

};

Ahorcado.prototype.dibujar = function() {

  var dibujo = this.contexto;

  //Dibujar poste
  dibujo.beginPath();
    dibujo.lineWidth = 15;
    dibujo.strokeStyle = "#000000";

    dibujo.moveTo(150, 100);
    dibujo.lineTo(150, 50);
    dibujo.lineTo(350, 50);
    dibujo.lineTo(350, 350);
    dibujo.moveTo(300, 350);
    dibujo.lineTo(400, 350);
    dibujo.stroke();
  dibujo.closePath();

  if (this.intentos > 0) {
    //intentos = 1 = rostro
    dibujo.beginPath();
      dibujo.lineWidth = 5;
      dibujo.strokeStyle = "#FF0022";
      dibujo.arc(150, 140, 40, 0, Math.PI * 2);
      dibujo.stroke();
    dibujo.closePath();
  }

  if (this.intentos > 1) {
    //intentos = 2 = brazos
    dibujo.beginPath();
      dibujo.lineWidth = 5;
      dibujo.strokeStyle = "#FF0022";
      dibujo.moveTo(150, 180);
      dibujo.lineTo(150, 250);
      dibujo.stroke();
    dibujo.closePath();
  } 
 
  if (this.intentos > 2) {
    // intentos = 3 = torso
    dibujo.beginPath();
      dibujo.lineWidth = 5;
      dibujo.strokeStyle = "#FF0022";
      dibujo.moveTo(120, 220);
      dibujo.lineTo(150, 180);
      dibujo.lineTo(180, 220);
      dibujo.stroke();
    dibujo.closePath();
  }

  if (this.intentos > 3) {
    // intentos = 4 = piernas
    dibujo.beginPath();
      dibujo.lineWidth = 5;
      dibujo.strokeStyle = "#FF0022";
      dibujo.moveTo(120, 280);
      dibujo.lineTo(150, 250);
      dibujo.lineTo(180, 280);
      dibujo.stroke();
    dibujo.closePath();
  }

  if (this.intentos > 4) {
    // intentos = 5 = ojos y boca
    // x_x

    //ojos
    dibujo.beginPath();
      dibujo.lineWidth = 5;
      dibujo.strokeStyle = "#FF0022";

      dibujo.moveTo(125, 120);
      dibujo.lineTo(145, 145);
      dibujo.moveTo(145, 120);
      dibujo.lineTo(125, 145);

      dibujo.moveTo(155, 120);
      dibujo.lineTo(175, 145);
      dibujo.moveTo(175, 120);
      dibujo.lineTo(155, 145);
      dibujo.stroke();
    dibujo.closePath();
    
    // boca
    dibujo.beginPath();
      dibujo.lineWidth = 5;
      dibujo.strokeStyle = "#FF0022"
      dibujo.lineWidth = 5;
      dibujo.arc(150, 175, 20, 1.1 * Math.PI, 1.9 * Math.PI);
      dibujo.stroke();
    dibujo.closePath();
  }

  
};

Ahorcado.prototype.trazar = function() {
  this.intentos++;
  if (this.intentos >= this.maximo) {
    this.vivo = false;
  }
  
  this.dibujar();

};

function agregarLetra(letra) {
  if (!letra.value){
    alert("Debe introducir una letra");
    return;
  }
  var l = letra.value[0].toUpperCase(); 

  console.log(palabra);
  //si encuentra letra 
  if (palabra.match(l)) {
    for (i in palabra) {
      if (l == palabra[i]) {
        letrasUsuario[i] = l;
      }
    }
  } else {
    usuario.trazar();
    alert("No se encontro " + l + "\n" +
        "Quedan " + (usuario.maximo - usuario.intentos) + " intentos");
  }
  //Reiniciar valor de letra
  letra.value = '';

}

function mostrarPista(pista) {
  pista.innerText = letrasUsuario.join(' ');
}

function init() {
  var canvas = document.getElementById('c');
  canvas.width = 500;
  canvas.height = 400;

  var contexto = canvas.getContext('2d');
  usuario = new Ahorcado(contexto);

  var boton = document.getElementById("boton");

  pista = document.getElementById('pista');
  letra = document.getElementById("letra");
  mostrarPista(pista);

  boton.addEventListener("click", function() {
    agregarLetra(letra);
    mostrarPista(pista);
    if (!usuario.vivo){
      alert("Has perdido! :(\nRecarga la página para jugar de nuevo");
      letra.value = 'RECARGA LA PAGINA';
      letra.readOnly = true;
    }
    if (palabra === letrasUsuario.join('')) {
      alert("Haz ganado! :D");
    }
  });

}


